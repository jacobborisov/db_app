from __future__ import print_function

import logging


class Backend:
    def __init__(self):
        self._storage = dict()

    def get(self, key, unset_value=None):
        return self._storage.get(key, unset_value)

    def set(self, key, value):
        self._storage[key] = value

    def unset(self, key):
        self._storage.pop(key, None)

    def counts(self, value):
        return self._storage.values().count(value)

    def find(self, value):
        return [k for k, v in self._storage.viewitems() if v == value]


class BackendWithTransactions:

    """
    KV storage implemented as a chain of "snapshots"
    (actually simple map (dict) objects), where first snapshot
    is the master snapshot, contains only committed data,
    and chained snapshots contain only "differential" data
    modified in a corresponded (but not committed yet) transaction.

    In a case when transactions are committed, all descendant snapshots
    in the chain sequentially merge into master snapshot. It's possible
    to specify if you want to keep deleted values state
    in the master snapshot or not, using keep_deleted_keys parameter.
    """

    MASTER_IDX, CHILD_IDX, CURRENT_IDX = 0, 1, -1

    def __init__(self, keep_deleted_keys=False):
        self._storage = [dict()]
        self._removed = object()  # unique obj which can't be found elsewhere
        self.keep_deleted_keys = keep_deleted_keys

    def has_uncommitted_transactions(self):
        return len(self._storage) > 1

    @property
    def _cur_snapshot(self):
        return self._storage[self.CURRENT_IDX]

    @property
    def _master_snapshot(self):
        return self._storage[self.MASTER_IDX]

    def _snapshots(self):
        for snapshot in reversed(self._storage):
            yield snapshot

    def _is_deleted(self, value):
        return value is self._removed

    def get(self, key, unset_value=None):
        for snapshot in self._snapshots():
            try:
                value = snapshot[key]
            except KeyError:
                continue
            else:
                if self._is_deleted(value):
                    break
                return value
        return unset_value

    @property
    def _clean_current_snapshot(self):
        return not self.keep_deleted_keys and \
            not self.has_uncommitted_transactions()

    def set(self, key, value):
        if self._is_deleted(value) and self._clean_current_snapshot:
            self._cur_snapshot.pop(key, None)
        else:
            self._cur_snapshot[key] = value

    def unset(self, key):
        self.set(key, self._removed)

    def counts(self, value):
        return sum(1 for _ in self._find(value))

    def _find(self, value):
        found_keys = []
        for snapshot in self._snapshots():
            for k, v in snapshot.viewitems():
                if k not in found_keys:
                    found_keys.append(k)
                    if v == value:
                        yield k

    def find(self, value):
        return list(self._find(value))

    def begin(self):
        self._storage.append(dict())

    def rollback(self):
        if not self.has_uncommitted_transactions():
            return
        self._drop_snapshot()

    def _drop_snapshot(self, index=CURRENT_IDX):
        self._storage.pop(index)

    def _merge_to_master(self, source_idx):
        child = self._storage[source_idx]

        if self.keep_deleted_keys:
            self._master_snapshot.update(child)
        else:
            for k, v in child.viewitems():
                if self._is_deleted(v):
                    self._master_snapshot.pop(k)
                else:
                    self._master_snapshot[k] = v

    def commit(self):
        while self.has_uncommitted_transactions():
            self._merge_to_master(self.CHILD_IDX)
            self._drop_snapshot(self.CHILD_IDX)


class DB_Client:
    class cmds:
        GET = 'GET'
        SET = 'SET'
        UNSET = 'UNSET'
        COUNTS = 'COUNTS'
        FIND = 'FIND'
        BEGIN = 'BEGIN'
        ROLLBACK = 'ROLLBACK'
        COMMIT = 'COMMIT'

    class values:
        NULL = 'NULL'

    args_len = {
        cmds.GET: 1,
        cmds.SET: 2,
        cmds.UNSET: 1,
        cmds.COUNTS: 1,
        cmds.FIND: 1,
        cmds.BEGIN: 0,
        cmds.ROLLBACK: 0,
        cmds.COMMIT: 0
    }

    def __init__(self):
        self._backend = BackendWithTransactions(False)

    def run_cmd(self, cmd, *args):
        if len(args) != self.args_len[cmd]:
            logging.error("Invalid number of arguments"
                          " (%d instead of %d) for command: %s",
                          len(args), self.args_len[cmd], cmd)
            return
        if cmd == self.cmds.GET:
            return self._backend.get(args[0], self.values.NULL)
        elif cmd == self.cmds.SET:
            self._backend.set(args[0], args[1])
        elif cmd == self.cmds.UNSET:
            self._backend.unset(args[0])
        elif cmd == self.cmds.COUNTS:
            return str(self._backend.counts(args[0]))
        elif cmd == self.cmds.FIND:
            return "\n".join(self._backend.find(args[0]))
        elif cmd == self.cmds.BEGIN:
            self._backend.begin()
        elif cmd == self.cmds.ROLLBACK:
            self._backend.rollback()
        elif cmd == self.cmds.COMMIT:
            self._backend.commit()


if __name__ == '__main__':

    import cmd
    import re
    import sys

    logging.basicConfig(level=logging.FATAL, stream=sys.stderr)

    db = DB_Client()

    def execute_cmd(name, args_line, explicit_empty_result=False):
        logging.debug("Args line: \"%s\", len: %s", args_line, len(args_line))
        args_line = args_line.strip()
        args = re.split(r'\s+', args_line) if args_line else []
        result = db.run_cmd(name, *args)
        if result is not None and (not(type(result) is str) or result):
            print(result)
        elif explicit_empty_result:
            print()

    class CmdShellBase(cmd.Cmd):

        def precmd(self, line):
            line = line.strip().partition(' ')
            return line[0].upper() + line[1] + line[2]

        def do_EOF(self, *args):
            return True

        do_END = do_EOF

    commands = {'do_%s' % name:
                (lambda name: (lambda *args: execute_cmd(name, args[1])))(name)
                for name in vars(db.cmds) if not name.startswith('_')}

    CmdApp = type("CmdApp", (CmdShellBase, object), commands)
    CmdApp().cmdloop()
