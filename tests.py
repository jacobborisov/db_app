from app import DB_Client

import unittest


class TestTranscations(unittest.TestCase):

    def test_main(self):
        db = DB_Client()

        self.assertEqual(db.run_cmd(db.cmds.GET, 'A'), 'NULL')
        db.run_cmd(db.cmds.SET, 'A', 'fff')
        self.assertEqual(db.run_cmd(db.cmds.GET, 'A'), 'fff')
        db.run_cmd(db.cmds.BEGIN)
        db.run_cmd(db.cmds.BEGIN)
        self.assertEqual(db.run_cmd(db.cmds.GET, 'A'), 'fff')
        db.run_cmd(db.cmds.UNSET, 'A')
        self.assertEqual(db.run_cmd(db.cmds.GET, 'A'), 'NULL')
        db.run_cmd(db.cmds.BEGIN)
        db.run_cmd(db.cmds.SET, 'A', 'zzz')
        db.run_cmd(db.cmds.SET, 'B', 'zzz')
        self.assertEqual(db.run_cmd(db.cmds.COUNTS, 'zzz'), '2')
        self.assertEqual(db.run_cmd(db.cmds.FIND, 'zzz'), 'A\nB')
        self.assertEqual(db.run_cmd(db.cmds.GET, 'A'), 'zzz')
        db.run_cmd(db.cmds.ROLLBACK)
        self.assertEqual(db.run_cmd(db.cmds.GET, 'A'), 'NULL')
        db.run_cmd(db.cmds.ROLLBACK)
        self.assertEqual(db.run_cmd(db.cmds.GET, 'A'), 'fff')
        db.run_cmd(db.cmds.COMMIT)

    def test_example1(self):
        db = DB_Client()

        self.assertEqual(db.run_cmd(db.cmds.GET, 'A'), 'NULL')
        db.run_cmd(db.cmds.SET, 'A', '10')
        self.assertEqual(db.run_cmd(db.cmds.GET, 'A'), '10')
        self.assertEqual(db.run_cmd(db.cmds.COUNTS, '10'), '1')
        db.run_cmd(db.cmds.SET, 'B', '20')
        db.run_cmd(db.cmds.SET, 'C', '10')
        self.assertEqual(db.run_cmd(db.cmds.COUNTS, '10'), '2')
        db.run_cmd(db.cmds.UNSET, 'B')
        self.assertEqual(db.run_cmd(db.cmds.GET, 'B'), 'NULL')

    def test_example2(self):
        db = DB_Client()

        db.run_cmd(db.cmds.BEGIN)
        db.run_cmd(db.cmds.SET, 'A', '10')
        db.run_cmd(db.cmds.BEGIN)
        db.run_cmd(db.cmds.SET, 'A', '20')
        db.run_cmd(db.cmds.BEGIN)
        db.run_cmd(db.cmds.SET, 'A', '30')
        self.assertEqual(db.run_cmd(db.cmds.GET, 'A'), '30')
        db.run_cmd(db.cmds.ROLLBACK)
        self.assertEqual(db.run_cmd(db.cmds.GET, 'A'), '20')
        db.run_cmd(db.cmds.COMMIT)
        self.assertEqual(db.run_cmd(db.cmds.GET, 'A'), '20')


if __name__ == '__main__':
    unittest.main()
